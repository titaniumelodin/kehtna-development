import java.util.Scanner;

public class Calculator {
	public static void main(String[] args) {
		String a = args[0];
		String b = args[2];
		String c = args[1];

		if (args.length == 0) {
			handleUserInput();
			System.out.println(calcChoices(a, b, c));
		} else {
			System.out.println( calcChoices(a, b, c));

		}
	}

	

	public static String calcChoices(String a, String b, String c) {
		System.out.println("test1 " + a + " test2 " + b);
		int a2 = Integer.parseInt(a);
		int b2 = Integer.parseInt(b);
		
		if (c.equals("+")) {
			return ("" + sum(a2, b2));
		} else if (c.equals("-")) {
			return ("" + substract(a2, b2));
		} else if (c.equals("x")) {
			return ("" + multiply(a2, b2));
		} else if (c.equals("/")) {
			return("" + divide(a2, b2));
		}
		return "meh";
	}

	public static void handleUserInput() {
		Scanner scanner = new Scanner(System.in);
		String a = scanner.next();
		System.out.println("First number: " + a);
		String operator = scanner.next();
		System.out.println("Operator: " + operator);
		String b = scanner.next();
		System.out.println("Second number: " + b);
	}

	public static int sum(int a, int b) {
		return (a + b);
	}

	public static int substract(int a, int b) {
		return (a - b);
	}

	public static int multiply(int a, int b) {
		return (a * b);
	}

	public static float divide(int a, int b) {
		return ((float)a / b);
	}
}