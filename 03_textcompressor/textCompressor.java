import java.util.Scanner;

public class textCompressor {
	public static void main(String[] args) {
		int counter = 1;
		String newStr = "";
		String text = "";

		if (args.length > 0) {
			for (int i=0;i<args.length;i++) {
				text += args[i] + " ";
			}

			text = text.trim();

		    for(int i=0;i<text.length();i++){
		        if (i>0) {
		        	char thisCh = text.charAt(i);
		        	char prevCh = text.charAt(i - 1);
		        	if (thisCh == prevCh) {
		        		counter += 1;
		        	} else {
		        		if (counter > 1) {
		        			newStr += counter + "" + prevCh;
		        			counter = 1;
		        		} else {
		        			newStr += prevCh;
		        		}
		        	}
		        }
		        if (i == text.length() - 1) {
			        if (counter > 1) {
			        	newStr += counter + "" + text.charAt(text.length() - 1);
			        } else {
			        	newStr += text.charAt(text.length() - 1);
			        }	        	
		        }

		    }			
		}



	    System.out.println(newStr);

	}
}