public class playerInput {
    public boolean checkInput(String x, String y, int size){
        try {
            int xInt = Integer.parseInt(x);
            int yInt = Integer.parseInt(y);
            if (xInt >= size || yInt >= size || xInt < 0 || yInt < 0) {
                throw new ArrayIndexOutOfBoundsException();
            }
            System.out.println("Bombs ahoy captain!");
        } catch (NumberFormatException e1){
            System.out.println("Those were not coordinates captain!");
            return false;
        } catch (ArrayIndexOutOfBoundsException e2) {
            System.out.println("That's outta bounds captain!");
            return false;
        }
        return true;
    }
}
