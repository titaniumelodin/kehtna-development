//This class will draw and update the board the player sees
public class PlayerBoard {
    String[][] shownBoard = generateShownBoard(NavalBattle.DEFAULT_BOARD_SIZE); //Fills the table with *

    public void drawShownBoard(int boardSize,int[][] board) { //THIS IS THE FIRST RUN VERSION OF THE METHOD
        int counterx = 0;
        int countery = 0;

        for (int l = 0; l < boardSize; l++){ //colum numbers
            System.out.print( " " + counterx);
            counterx++;
        }

        System.out.print("\n ╔"); //"╔═╦═╦═╦═╗"
        for (int i = 1; i < boardSize; i++) {
            System.out.print("═╦");
        }
        System.out.print("═╗\n");
        for (int j = 1; j < boardSize; j++) {
            System.out.print(countery); //row numbers
            countery++;
            for (int k = 0; k < boardSize; k++) { //"║ ║ ║ ║ "
                System.out.print( "║"+"*");//board[j][k]
            }
            System.out.print("║\n ╠"); // " ║"
            for (int m=1; m < boardSize; m++) { //"╠═╬═╬═╬═╬"
                System.out.print("═╬");
            }
            System.out.print("═╣\n");
        }

        System.out.print(countery); //last row number

        for (int k = 0; k < boardSize; k++) { //"║ ║ ║ ║ "
            System.out.print("║"+"*");//board[2][k]
        }
        System.out.print("║\n"); // " ║"

        System.out.print(" ╚");

        for (int n = 1; n < boardSize; n++) {
            System.out.print("═╩");
        }
        System.out.print("═╝");
    }


    public void drawShownBoard(int boardSize,int[][] board, String x, String y, String result) { //THIS IS THE VERSION OF THE METHOD USED AFTER THE FIRST RUN
        int counterx = 0;
        int countery = 0;
        int xInt = Integer.parseInt(x);
        int yInt = Integer.parseInt(y);


        //replace the * with that coordinate with either 1 or 0

        for (int l = 0; l < boardSize; l++){ //colum numbers
            System.out.print( " " + counterx);
            counterx++;
        }
        if (result != "2") {
            shownBoard[xInt][yInt] = result;
        }
        System.out.print("\n ╔"); //"╔═╦═╦═╦═╗"
        for (int i = 1; i < boardSize; i++) {
            System.out.print("═╦");
        }
        System.out.print("═╗\n");
        for (int j = 1; j < boardSize; j++) {
            System.out.print(countery); //row numbers
            countery++;
            for (int k = 0; k < boardSize; k++) { //"║ ║ ║ ║ "
                System.out.print( "║"+shownBoard[k][j - 1]);

            }
            System.out.print("║\n ╠"); // " ║"
            for (int m=1; m < boardSize; m++) { //"╠═╬═╬═╬═╬"
                System.out.print("═╬");
            }
            System.out.print("═╣\n");
        }

        System.out.print(countery); //last row number

        for (int k = 0; k < boardSize; k++) { //"║ ║ ║ ║ "
            System.out.print("║"+shownBoard[k][shownBoard.length - 1]);//board[2][k]
        }
        System.out.print("║\n"); // " ║"

        System.out.print(" ╚");

        for (int n = 1; n < boardSize; n++) {
            System.out.print("═╩");
        }
        System.out.print("═╝");
    }
    private String[][] generateShownBoard(int boardSize) {
        String[][] shownBoard = new String[boardSize][boardSize];

        for (int i = 0; i < boardSize; i++){
            for (int j = 0; j < boardSize; j++) {
                shownBoard[i][j] = "*";
            }
        }
        return shownBoard;
    }
}





