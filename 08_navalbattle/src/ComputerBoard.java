//This class generates the backend board (aka where the boats are) in the form of either 0s and 1s or Trues and Falses
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
//this generates the board matrix and fills it with 0s and 1s
public class ComputerBoard {
    public int[][] HiddenBoard(int size, int ships){ //this generates the board matrix and fills it with 0s and 1s
            int[][] board = new int[size][size];
            Integer[] board1D = new Integer[size*size];

            for (int i = 0; i < size * size; i++) {
                board1D[i] = 0;
            }

           for(int i = 0; i < ships; i++){ //MIGHT BE WHY IT BREAKS
                board1D[i] = 1;
           }

            Collections.shuffle(Arrays.asList(board1D), new Random());

            for(int i=0; i < size;i++) {
                for (int j = 0; j < size; j++){
                    board[i][j] = board1D[(j * size) + i];
                }
            }
            return board;
    }
    public String checkBoard(String x, String y, int[][] board) {
        int xInt = Integer.parseInt(x);
        int yInt = Integer.parseInt(y);
        String hitOrMiss;
        if (board[xInt][yInt] == 2){
            System.out.println("we already shot there captain!");
        } else if (board[xInt][yInt] == 1) {
            System.out.println("We hit one captain!");
            board[xInt][yInt] = 2;
            return "1";
        }  else if (board[xInt][yInt] == 0){
            System.out.println("We missed captain!");
            board[xInt][yInt] = 2;
            return "0";
        }
        return "2";
    }
}

