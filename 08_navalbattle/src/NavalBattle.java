import java.io.*;
import java.util.*;


public class NavalBattle implements Runnable {
    private int[][] board;
    public static final int DEFAULT_BOARD_SIZE = 5;
    private static final int DEFAULT_SHIP_NUMBER = 16;
    private static final String PROGRESS_FILENAME = "progress.txt";

    private PlayerBoard playerBoard;
    private ComputerBoard computerBoard;
    private playerInput playerInput;

    private boolean isGameRunning = false;

    public static void main(String[] args) {
        NavalBattle app = new NavalBattle(DEFAULT_BOARD_SIZE, DEFAULT_SHIP_NUMBER);
        Thread thread = new Thread(app);
        //app.saveBoard();
        thread.start();
        //app.loadProgress();
    }

    public NavalBattle(int size, int ships){

        if(DEFAULT_BOARD_SIZE * DEFAULT_BOARD_SIZE < DEFAULT_SHIP_NUMBER) {
            System.out.println("There are more ships here than there's sea captain! I think we are doomed!");
            System.exit(1);
        }

        playerBoard = new PlayerBoard();
        computerBoard = new ComputerBoard();
        playerInput = new playerInput();
        board = computerBoard.HiddenBoard(size, ships);

        playerBoard.drawShownBoard(DEFAULT_BOARD_SIZE, board);

        isGameRunning = true;
    }



//SAVE PLAYER PROGRESS BASE FOR LATER USE
    private void saveBoard() {
        try {
            FileWriter fileWriter = new FileWriter(PROGRESS_FILENAME);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("a");
            for (int i = 0; i < 55; i++) { //board.length
                for (int j = 0; j < 55; j++) { //board.length[0]
                    bufferedWriter.write(String.valueOf("a"));

                    if (i < board.length - 1) {
                        bufferedWriter.newLine();
                    }
                }
            }
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadProgress() {
        List<String> lines = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader(PROGRESS_FILENAME);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = null;
            while ((line = bufferedReader.readLine())!= null) {
                lines.add(line);
                System.out.println("Reading line: " + line);
            }
            bufferedReader.close();
        } catch(FileNotFoundException e) {
                e.printStackTrace();
        } catch(IOException e) {
                e.printStackTrace();
        }
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            for (int j=0; j < line.length(); j++) {
                char value = line.charAt(j);
                System.out.print(value);
            }
            System.out.println("");
        }
    }

    public void run(){

        while (isGameRunning) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please input the coordinates you'd like to bomb captain! \nX coordinate: ");
            String x = scanner.next();
            System.out.println("Y coordinate: ");
            String y = scanner.next();

            if (playerInput.checkInput(x, y, DEFAULT_BOARD_SIZE)) {
                String hitOrMiss = computerBoard.checkBoard(x, y, board);
                playerBoard.drawShownBoard(DEFAULT_BOARD_SIZE, board, x, y, hitOrMiss);
            }

        }
    }

}



/*
TODO Primary
DONE: Sort out the X and Y axis: Figure out why the X and Y axis are flipped and "X" is -1 position on the board
DONE: Update player board: When the user inputs the coordinates remember that coordinate on the player board by permanently showing either 1 or 0 there.
DONE: Fix last row: It's doing some weeeird stuff...
DONE: Remove the 2s: Players don't need to see that
DONE: Different sizes might crash the program: Figure out why and fix it
DONE: Fix the board: It's only showing 0s
DONE: First row: First row is always 0

End conditions: Set up victory/loss conditions so if all the ships are hit the game ends

Save and load: I theoretically have all the code in a working form, now figure out how to implement it



TODO Secondary

Comment: Need I say more?
Clean up the code: Yeah...
Game status: How many bombs are there? How many tries do you still have? This is really much a nice to have ^^'
Board size: Let the player choose the board size within logical size. This is the most nice to have thing I've got. Man I actually miss game development I need to dapple in it more : P
*/