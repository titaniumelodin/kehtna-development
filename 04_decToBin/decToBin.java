
//TO BE CONTINUED

public class decToBin {
	public static void main(String[] args) {
		int dec = Integer.parseInt(args[0]);
		String bin = "";
		while (dec != 0) {
			dec = dec / 2;
			if ( (dec & 1) == 0) {
				bin += "0";
			} else {
				bin += "1";
			}
		}
		System.out.println(bin);
	}
}