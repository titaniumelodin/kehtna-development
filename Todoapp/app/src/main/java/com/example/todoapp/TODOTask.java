package com.example.todoapp;

import java.util.Calendar;

public class TODOTask {

    public String key;
    public String description;
    public boolean completed;
    public long createdDate;

    public TODOTask() {
    }

    public TODOTask(String description) {
        this.description = description;
        completed = false;
        createdDate = Calendar.getInstance().getTime().getTime();
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;

    }
}