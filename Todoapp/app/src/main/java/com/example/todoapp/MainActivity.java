package com.example.todoapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


//
public class MainActivity extends AppCompatActivity {
    private List<TODOTask> tasks;
    private ListView listView;
    public ArrayAdapter<TODOTask> listAdapter;

    private Button add;
    private EditText inputTask;
    private RESTService restService;
    private DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = FirebaseDatabase.getInstance().getReference();

        String email = "tiigrikoer595@gmail.com";
        String password = "TempKehtnaPass";
        //signUp(email, password);
        logIn(email, password);


        add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = inputTask.getText().toString();
                if (description.isEmpty() == false) {
                    TODOTask task = new TODOTask(description);
                    listAdapter.add(task);
                    addTask(task);


                    inputTask.setText("");
                    listView.setSelection(listAdapter.getCount() - 1);
                    listAdapter.clear();
                }
            }
        });


        inputTask= findViewById(R.id.input_task);

        listView = findViewById(R.id.list_tasks);

        tasks = new ArrayList<>();
//        tasks.add(new TODOTask("Call Mother"));
//        tasks.add(new TODOTask("Survive the gauntlet"));
//        tasks.add(new TODOTask("Kill Dyonesus"));
//        tasks.add(new TODOTask("Do the Moo dance"));
//        tasks.add(new TODOTask("AAAAAAAAAAAHHHHHHHHHHHH!!!!!"));

        //listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tasks);
        listAdapter = new CustomArrayAdapter(this, tasks);

        listView.setAdapter(listAdapter);

//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(" http://a055ffdc.ngrok.io")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        restService = retrofit.create(RESTService.class);
//        getTasksFromServer();

        //CustomArrayAdapter.viewHolder.completed.setOnClickListener

    }
//
//    private void getTasksFromServer() {
//        Call<List<TODOTask>> call = restService.getTasks();
//        call.enqueue(new Callback<List<TODOTask>>() {
//            @Override
//            public void onResponse(Call<List<TODOTask>> call, Response<List<TODOTask>> response) {
//                List<TODOTask> tasks = response.body();
//                if (tasks != null && tasks.size() > 0) {
//                    for (TODOTask task : tasks) {
//                        listAdapter.add(task);
//                    }
//                }
//            }
//            @Override
//            public void onFailure(Call<List<TODOTask>> call, Throwable t) {
//                Log.d("TODOApp", t.getMessage());
//            }
//        });
//    }
//
//    public void updateTask(TODOTask task) {
//        Call<TODOTask> call = restService.updateTask(task.id, task);
//        call.enqueue(new Callback<TODOTask>() {
//            @Override
//            public void onResponse(Call<TODOTask> call, Response<TODOTask> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<TODOTask> call, Throwable t) {
//                Log.d("TODOApp", t.getMessage());
//            }
//        });
//    }

//    public void addTask(TODOTask task) {
//        Call<TODOTask> call = restService.addTask(task);
//        call.enqueue(new Callback<TODOTask>() {
//            @Override
//            public void onResponse(Call<TODOTask> call, Response<TODOTask> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<TODOTask> call, Throwable t) {
//                Log.d("TODOApp", t.getMessage());
//            }
//        });
//    }
//
//    public void deleteTask(int id) {
//        Call<TODOTask> call = restService.deleteTask(id);
//        call.enqueue(new Callback<TODOTask>() {
//            @Override
//            public void onResponse(Call<TODOTask> call, Response<TODOTask> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<TODOTask> call, Throwable t) {
//                Log.d("TODOApp", t.getMessage());
//            }
//        });
//
//    }
    public void signUp(String email, String password) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Account Created! Welcome to hell buddy!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Account creation FAILED! Baaaaaka!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void logIn(String email, String password) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Eh... Login successful!... I guess... >~<'", Toast.LENGTH_SHORT).show();
                            loadAllTasks();
                        } else {
                            Toast.makeText(getApplicationContext(), "Login FAILED! Baaaaaka!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    private void loadAllTasks() {
        database.child("tasks").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listAdapter.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    TODOTask task = snapshot.getValue(TODOTask.class);
                    listAdapter.add(task);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void addTask (TODOTask task) {
        task.key = database.child("tasks").push().getKey();
        database.child("tasks").child(task.key).setValue(task);
    }

    public void updateTask(TODOTask task) {
        database.child("tasks").child(task.key).setValue(task);
    }

    public void deleteTask(TODOTask task) {
        database.child("tasks").child(task.key).setValue(null);
    }
}


/*
TODO primary
DONE: updateTask: I got desperate so I checked the other folks repos in the second year and I found a solution for that in only Taavi's repo and that was the old version :/
DONE: Update the view: when I press the delete button it deletes the item and pushes it onto the list. Apperantly there was a single line of code that could fix that UI problem if I remember correctly.

TODO secondary
(if I have the time which I probably don't tbh)
DONE: Logic check: If the field is empty you can't input the thing. (this is more like in the crack between primary and secondary)
Comment the code: So I could finally understand the dang thing...
Clean up the code: I really reeaaaallyy REALLLY should do that but in all honesty probably ain't gonna do that.
 */