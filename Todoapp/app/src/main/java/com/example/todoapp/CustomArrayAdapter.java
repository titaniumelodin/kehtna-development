package com.example.todoapp;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class CustomArrayAdapter extends ArrayAdapter<TODOTask> {

    private MainActivity mainActivity;

    public CustomArrayAdapter(MainActivity mainActivity, List<TODOTask> tasks) {
        super(mainActivity.getApplicationContext(), 0, tasks);
        this.mainActivity = mainActivity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final TaskViewHolder viewHolder;
        final TODOTask task =  getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_task, parent, false);
            viewHolder = new TaskViewHolder();
            viewHolder.id = convertView.findViewById(R.id.idSpot);
            viewHolder.description = convertView.findViewById(R.id.text_description);
            viewHolder.date = convertView.findViewById(R.id.text_date);
            viewHolder.completed = convertView.findViewById(R.id.completed);
            viewHolder.remove = convertView.findViewById(R.id.remove);



            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TaskViewHolder) convertView.getTag();
        }

       // TODOTask task = getItem(position);

        viewHolder.id.setText("" + task.key);
        viewHolder.description.setText("" + task.description);
        viewHolder.completed.setChecked(task.completed);
        viewHolder.completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                task.setCompleted(viewHolder.completed.isChecked());
                mainActivity.updateTask(task);
                //mainActivity.listAdapter.clear();
            }
        });

                viewHolder.date.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)", task.createdDate));
        viewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.deleteTask(task);
                //mainActivity.listAdapter.clear();



            }
        });

        return convertView;
    }
    public class TaskViewHolder {
        public TextView id;
        public TextView date;
        public TextView description;
        public CheckBox completed;
        public Button remove;

    }
}
